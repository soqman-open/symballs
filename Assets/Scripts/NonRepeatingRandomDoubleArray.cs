﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class NonRepeatRandomDoubleArray
{
	private readonly List<Vector2Int> _vectors = new List<Vector2Int>();
	public NonRepeatRandomDoubleArray(int x, int y)
	{
		for (var i = 0; i < x; i++)
		{
			for (var j = 0; j < y; j++)
			{
				_vectors.Add(new Vector2Int(i, j));
			}
		}
	}

	public Vector2Int? GetRandom()
	{
		if (_vectors.Count == 0) return null;
		var order = Random.Range(0,_vectors.Count);
		var vector = _vectors[order];
		_vectors.RemoveAt(order);
		return vector;
	}
}
