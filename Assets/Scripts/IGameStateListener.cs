﻿public interface IGameStateListener
{
	void OnGameStateChanged(GameStates gameState);
}
