﻿using System;
using System.Collections;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions.Must;
using UnityEngine.Events;
using UnityEngine.UI;

public class UnitBehaviour : MonoBehaviour, IAnimable
{
    [SerializeField]private float _animationSpeed=10f;
    [SerializeField]private int _tipTimer;
    
    public Unit Unit { get; private set; }
    private Coroutine _timer;

    public void SetMarkedAsPossible(bool value)
    {
        _isMarkedAsPossible = value;
        if (_isMarkedAsPossible)
        {
            _timer = StartCoroutine(CulldownTimer(_tipTimer));
        }
        else
        {
            OnPossibleStopEvent.Invoke();
            if(_timer!=null)StopCoroutine(_timer);
        }
    }

    public bool GetIsPossible()
    {
        return _isMarkedAsPossible;
    }

    public bool IsMarkedForKill;
    private bool _isMarkedAsPossible;
    public UnityEvent OnDieEvent;
    public UnityEvent OnPossibleEvent;
    public UnityEvent OnPossibleStopEvent;
    private float _sizeCoeff;
    
    public void Init(Unit unit)
    {
        Unit = unit;
        var image = GetComponent<UnityEngine.UI.Image>();
        image.color = Unit.UnitType.Color;
        image.sprite = Unit.UnitType.SpriteImage;
        _sizeCoeff = 1/Area.Instance.SizeCoeff;
        switch (unit.Special)
        {
            case Unit.SpecialUnit.None:
                break;
            case Unit.SpecialUnit.Bomb3:
                Debug.Log("bomb3");
                ((RectTransform) transform).sizeDelta *= 1.3f;
                break;
            case Unit.SpecialUnit.XRemover:
                Debug.Log("xremover");
                image.type = Image.Type.Filled;
                image.fillMethod = Image.FillMethod.Horizontal;
                image.fillAmount = 0.5f;
                break;
            case Unit.SpecialUnit.Yremover:
                Debug.Log("yremover");
                image.type = Image.Type.Filled;
                image.fillMethod = Image.FillMethod.Vertical;
                image.fillAmount = 0.5f;
                break;
            case Unit.SpecialUnit.Rocket:
                Debug.Log("rocket");
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

    }

    private IEnumerator CulldownTimer(int sec)
    {
        yield return new WaitForSeconds(sec);
        OnPossibleEvent.Invoke();
    }

    public void Kill()
    {
        StartSomeAnimation();
        OnDieEvent.Invoke();
    }
    
    public void DestroyObject()
    {
        StopSomeAnimation();
        Destroy(gameObject);
    }

    public IEnumerator MoveEnumerator(Node node)
    {
        StartSomeAnimation();
        while (Vector2.Distance(node.transform.position,transform.position)>1)
        {
            transform.SetPositionAndRotation(Vector2.MoveTowards(transform.position, node.transform.position, _animationSpeed*Time.deltaTime*_sizeCoeff),Quaternion.identity);
            yield return null; 
        }
        StopSomeAnimation();
    }

    private void StartSomeAnimation()
    {
        GameStateManager.Instance.RegisterAnimation(this);
    }
    private void StopSomeAnimation()
    {
        GameStateManager.Instance.UnRegisterAnimation(this);
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
        if (GameStateManager.Instance!=null)
        GameStateManager.Instance.UnRegisterAnimation(this);
    }
}
