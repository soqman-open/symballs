﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Area : Singleton<Area>, IGameStateListener
{
	[Header ("Transform")]
	[SerializeField] private RectTransform _areaTransform;
	[SerializeField] private Transform _unitsParentTransform;
	[SerializeField] private Transform _unitsHideParentTransform;
	
	[Header ("Prefabs")]
	[SerializeField] private GameObject _unitObject;
	[SerializeField] private GameObject _nodeObject;
	[SerializeField] private GameObject _emptyObject;
	
	[Header ("Settings")]
	[SerializeField] private int _areaMaxSize = 9;
	[SerializeField] private int _areaMinSize = 3;
	[SerializeField] private List<UnitType> _unitTypes;
	
	public static Node[,] Matrix { get; private set; }
	
	private float _nodeSizePx;
	private int _unitsCounter;
	private int _currentX;
	private int _currentY;
	
	public float SizeCoeff { get; private set; }

	private GameStates _currentGameState=GameStates.EditMode;
	
	private void Start()
	{
		GameStateManager.Instance.RegisterGameStateListener(this);
		InitEditMode();
	}

	private void OnDestroy()
	{
		if (GameStateManager.Instance != null)GameStateManager.Instance.UnRegisterGameStateListener(this);
	}

	private void InitEditMode()
	{
		Matrix = new Node[_areaMaxSize,_areaMaxSize];
		_nodeSizePx = _areaTransform.rect.width / _areaMaxSize;
		SizeCoeff = ((RectTransform) _nodeObject.transform).rect.width / _nodeSizePx;
		GenerateMatrix(_areaMaxSize,_areaMaxSize);
	}

	public void OnChangeSliders(Slider slider)
	{
		if (slider.direction == Slider.Direction.LeftToRight || slider.direction == Slider.Direction.RightToLeft)
		{
			GenerateMatrix((int)slider.value,_currentY);
		}
		else if (slider.direction == Slider.Direction.BottomToTop || slider.direction == Slider.Direction.TopToBottom)
		{
			GenerateMatrix(_currentX,(int)slider.value);
		}
	}

	private void CleanBoard()
	{
		foreach (Transform child in _areaTransform.transform)
		{
			Destroy(child.gameObject);
		}
		foreach (Transform child in _unitsHideParentTransform.transform)
		{
			Destroy(child.gameObject);
		}
	}

	private void GenerateNode(int x, int y)
	{
		var go = Instantiate(_nodeObject, _areaTransform);
		var node = go.GetComponent<Node>();
		node.X = x;
		node.Y = y;
		Matrix[x, y] = node;
		PlaceNode(go,x,y);
	}
	
	public void GenerateEmptyNode(int x, int y)
	{
		var go = Instantiate(_emptyObject, _unitsHideParentTransform);
		Matrix[x, y] = null;
		PlaceNode(go,x,y);
	}

	private void PlaceNode(GameObject go, int x, int y)
	{
		var rTransform = go.GetComponent<RectTransform>();
		rTransform.sizeDelta=new Vector2(_nodeSizePx,_nodeSizePx);
		rTransform.localPosition = new Vector3(_nodeSizePx / 2 + x * _nodeSizePx, _nodeSizePx / 2 + y * +_nodeSizePx);
	}

	private static int GetUpperIndex(int x)
	{
		var upperIndex = 0;
		for (var y = 0; y < Matrix.GetLength(1); y++)
		{
			if (Matrix[x, y] != null) upperIndex = y;
		}
		return upperIndex;
	}

	private void GenerateRandomRectMatrix()
	{
		CleanBoard();
		var x = Random.Range(_areaMinSize, _areaMaxSize);
		var y = Random.Range(_areaMinSize, _areaMaxSize);
		GenerateMatrix(x,y);
	}

	private void GenerateMatrix(int x, int y)
	{
		CleanBoard();
		_currentX = x;
		_currentY = y;
		var xOffset = Matrix.GetLength(0) / 2-x / 2;
		var yOffset = Matrix.GetLength(1) / 2-y / 2;
		
		for (var i = 0; i < Matrix.GetLength(0); i++)
		{
			for (var j = 0; j < Matrix.GetLength(1); j++)
			{
				if (i < xOffset || i >= xOffset + x || j < yOffset || j >= yOffset + y)
				{
					GenerateEmptyNode(i,j);
				}
				else
				{
					GenerateNode(i,j);
				}
			}
		}
	}
	
	private IEnumerator Gravitate()
	{
		for (var i = 0; i < Matrix.GetLength(1); i++)
		{
			var newCount = 0;
			for (var j = 0; j < Matrix.GetLength(0); j++)
			{
				
				if (Matrix[i, j] == null)continue;
				if (Matrix[i, j].IsEngaged)
				{
					Matrix[i, j].GetUnitBehaviour().SetMarkedAsPossible(false);
				}
				else
				{
					for (var k = 1; k <Matrix.GetLength(1)-j+1; k++)
					{
						if (j + k == Matrix.GetLength(0))
						{
							newCount++;
							var pos = new Vector2(Matrix[i, j].transform.position.x,
								Matrix[i, GetUpperIndex(i)].transform.position.y+_nodeSizePx*newCount);
							Matrix[i,j].SetUnitBehaviour(CreateRandomUnit(pos));
							StartCoroutine(Matrix[i, j].GetUnitBehaviour().MoveEnumerator(Matrix[i, j]));
							break;
						}
						if (!CheckExistAndHaveUnit(i, j + k))continue;
						Matrix[i,j].SetUnitBehaviour(Matrix[i,j+k].GetUnitBehaviour());
						Matrix[i, j].GetUnitBehaviour().SetMarkedAsPossible(false);
						Matrix[i,j+k].RemoveUnitBehaviour();
						StartCoroutine((Matrix[i, j]).GetUnitBehaviour().MoveEnumerator(Matrix[i,j]));
						break;
					}
				}
				
			}
		}
		while (_currentGameState == GameStates.Animated)
		{
			yield return null;
		}
		StartCoroutine(CheckMatches());
	}

	private bool CheckPossibility()
	{
		var r=new NonRepeatRandomDoubleArray(Matrix.GetLength(0),Matrix.GetLength(1));
		var possibleNotFounded=true;
		while (possibleNotFounded)
		{
			var v = r.GetRandom();
			if (v==null)
			{
				possibleNotFounded = false;
			}
			else
			{
				var value = v.Value;
				if (CheckExistAndHaveUnit(value.x, value.y) && Matrix[value.x, value.y].CheckPossible())
				{
					return true;
				}
			}
		}
		return false;
	}
	
	private IEnumerator RenewBoard()
	{
		CleanNodes();
		while (_currentGameState == GameStates.Animated)
		{
			yield return null;
		}
		StartCoroutine(Gravitate());
	}

	private void CleanNodes()
	{
		for (var i = 0; i < Matrix.GetLength(0); i++)
		{
			for (var j = 0; j < Matrix.GetLength(1); j++)
			{
				if(CheckExistAndHaveUnit(i,j)) Matrix[i,j].KillUnit();
			}
		}
	}
	
	private void CleanNodesImmediately()
	{
		StopAllCoroutines();
		foreach (Transform child in _unitsParentTransform.transform)
		{
			Destroy(child.gameObject);
		}
	}

	private IEnumerator CheckMatches()
	{
		for (var i = 0; i < Matrix.GetLength(0); i++)
		{
			for (var j = 0; j < Matrix.GetLength(1); j++)
			{
				if(CheckExistAndHaveUnit(i,j)) Matrix[i,j].CheckNeighbours();
			}
		}
		var killCounter=0;
		for (var i = 0; i < Matrix.GetLength(0); i++)
		{
			for (var j = 0; j < Matrix.GetLength(1); j++)
			{
				if (!CheckExistAndHaveUnit(i, j) || !Matrix[i, j].GetUnitBehaviour().IsMarkedForKill) continue;
				Matrix[i,j].KillUnit();
				killCounter++;
			}
		}	
		while (_currentGameState == GameStates.Animated)
		{
			yield return null;
		}
		if (killCounter > 0)
		{
			StartCoroutine(Gravitate());
			StatsHolder.Instance.PointsEarned(killCounter);
		}
		else
		{
			if (CheckPossibility())
			{
				GameStateManager.Instance.WaitActionTrigger();
			}
			else
			{
				StartCoroutine(RenewBoard());
			}
		}
	}

	private void KillAllByType(UnitType type)
	{
		var points=0;
		foreach (var node in Matrix)
		{
			if (node!=null && node.IsEngaged && node.GetUnitBehaviour().Unit.UnitType.Equals(type))
			{
				points++;
				node.KillUnitWithoutSpecial();
			}
		}
		StatsHolder.Instance.PointsEarned(points);
	}
	
	public void KillRange3(int x, int y)
	{
		var points=0;
		for (var i = x-1; i <= x+1; i++)
		{
			for (var j = y-1; j <= y+1; j++)
			{
				if (CheckExistAndHaveUnit(i, j))
				{
					points++;
					Matrix[i,j].KillUnit();
				}
			}
		}
		StatsHolder.Instance.PointsEarned(points);
	}

	public void KillX(int x)
	{
		var points=0;
		for (var i = 0; i < Matrix.GetLength(1); i++)
		{
			if (CheckExistAndHaveUnit(x, i))
			{
				Matrix[x,i].KillUnit();
				points++;
			}
		}
		StatsHolder.Instance.PointsEarned(points);
	}
	
	public void KillY(int y)
	{	
		var points=0;
		for (var i = 0; i < Matrix.GetLength(0); i++)
		{
			if (CheckExistAndHaveUnit(i, y))
			{
				Matrix[i,y].KillUnit();
				points++;
			}
		}
		StatsHolder.Instance.PointsEarned(points);
	}
	
	public bool CheckExistAndHaveUnit(int x, int y)
	{
		return IsMatrixIn(x, y) && Matrix[x, y] != null && Matrix[x, y].IsEngaged;
	}

	private bool IsMatrixIn(int x, int y)
	{
		return !(x < 0 || x >= _areaMaxSize || y < 0 || y >= _areaMaxSize);
	}
	
	private void GenerateNewUnits()
	{
		do
		{
			CleanNodesImmediately();
			GenerateUnits();
		} 
		while (!CheckPossibility());
	}

	private void GenerateUnits()
	{
		for (var i = 0; i < Matrix.GetLength(0); i++)
		{
			for (var j = 0; j < Matrix.GetLength(1); j++)
			{
				if(Matrix[i,j]!=null)Matrix[i,j].SetUnitBehaviour(CreateRandomUniqueUnit(i, j));
			}
		}
	}

	private UnitBehaviour CreateRandomUniqueUnit(int x,int y)
	{
		UnitType unitType;
		do{unitType = _unitTypes[Random.Range(1, _unitTypes.Count)];} while (Matrix[x, y].IsMatch3(unitType));
		var u = new Unit(_unitsCounter++, unitType);
		var position = Matrix[x, y].transform.position;
		var ub = Instantiate(_unitObject,position,Quaternion.identity,_unitsParentTransform).GetComponent<UnitBehaviour>();
		ub.Init(u);
		return ub;
	}
	
	private UnitBehaviour CreateRandomUnit(Vector2 pos)
	{
		var unitType = _unitTypes[Random.Range(1, _unitTypes.Count)];
		var u = new Unit(_unitsCounter++, unitType);
		var ub = Instantiate(_unitObject,pos,Quaternion.identity,_unitsParentTransform).GetComponent<UnitBehaviour>();
		ub.Init(u);
		return ub;
	}

	public UnitBehaviour CreateUnitWithSpecial(int x, int y, UnitType type, Unit.SpecialUnit specialUnit)
	{
		if (type == null) type = _unitTypes[0];
		var u = new Unit(_unitsCounter++, type);
		u.SetSpecial(specialUnit);
		var position = Matrix[x, y].transform.position;
		var ub = Instantiate(_unitObject,position,Quaternion.identity,_unitsParentTransform).GetComponent<UnitBehaviour>();
		ub.Init(u);
		return ub;
	}

	private void TrySwitch(Node first, Node second)
	{
		if (!first.IsEngaged || !second.IsEngaged) return;
		var u = first.GetUnitBehaviour();
		first.SetUnitBehaviour(second.GetUnitBehaviour());
		second.SetUnitBehaviour(u);
		if (CheckIsWhite(first, second))
		{
			StartCoroutine(SwitchWhite(first,second));
		}
		else if (first.IsMatch3() || second.IsMatch3())
		{
			StartCoroutine(SwitchValid(first,second));
		}
		else
		{
			StartCoroutine(SwitchInvalid(first, second));
		}
	}

	private bool CheckIsWhite(Node first, Node second)
	{
		return first.GetUnitBehaviour().Unit.UnitType == _unitTypes[0] ||second.GetUnitBehaviour().Unit.UnitType == _unitTypes[0];
	}

	public bool CanRemoveNode(int x,int y)
	{
		if (Matrix.Cast<Node>().Count() <= 9) return false;
		for (var i = 1; i < Matrix.GetLength(0)-1; i++)
		for (var j = 1; j < Matrix.GetLength(1)-1; j++)
		{
			var node = Matrix[i, j];
			if (node != null &&
			    Matrix[node.GetX() - 1, node.GetY()] != null &&
			    Matrix[node.GetX() + 1, node.GetY()] != null &&
			    Matrix[node.GetX(), node.GetY() - 1] != null &&
			    Matrix[node.GetX(), node.GetY() + 1] != null &&
			    Matrix[node.GetX() - 1, node.GetY() + 1] != null &&
			    Matrix[node.GetX() - 1, node.GetY() - 1] != null &&
			    Matrix[node.GetX() + 1, node.GetY() - 1] != null &&
			    Matrix[node.GetX() + 1, node.GetY() + 1] != null &&
			    !Utils.IsNeighbours9(Matrix[x, y], node) && !node.Equals(Matrix[x, y]))
				return true;
		}
		return false;
	}

	public void TrySwitch()
	{
		TrySwitch((Node)SelectManager.Instance.SelectedFirst,(Node)SelectManager.Instance.SelectedSecond);
		SelectManager.Instance.DeSelectAll();
	}

	private IEnumerator KillMatches(Node first, Node second)
	{
		var fut = first.GetUnitBehaviour().Unit.UnitType;
		var sut = second.GetUnitBehaviour().Unit.UnitType;
		var points = first.KillChain(fut) + second.KillChain(sut);
		StatsHolder.Instance.PointsEarned(points);
		while (_currentGameState==GameStates.Animated)
		{
			yield return null;
		}if(_currentGameState==GameStates.WaitAction)
		StartCoroutine(Gravitate());
	}

	private IEnumerator SwitchValid(Node first, Node second)
	{
		StatsHolder.Instance.Moves++;
		var f = first.GetUnitBehaviour();
		var s = second.GetUnitBehaviour();
		StartCoroutine(f.MoveEnumerator(first));
		StartCoroutine(s.MoveEnumerator(second));
		while (_currentGameState==GameStates.Animated)
		{
			yield return null;
		}
		StartCoroutine(KillMatches(first, second));
	}
	
	private IEnumerator SwitchWhite(Node first, Node second)
	{
		StatsHolder.Instance.Moves++;
		var f = first.GetUnitBehaviour();
		var s = second.GetUnitBehaviour();
		StartCoroutine(f.MoveEnumerator(first));
		StartCoroutine(s.MoveEnumerator(second));
		while (_currentGameState==GameStates.Animated)
		{
			yield return null;
		}
		var type = first.GetUnitBehaviour().Unit.Special == Unit.SpecialUnit.Rocket
			? second.GetUnitBehaviour().Unit.UnitType
			: first.GetUnitBehaviour().Unit.UnitType;
		KillAllByType(type);
		if(first.IsEngaged)first.KillUnitWithoutSpecial();
		if(second.IsEngaged)second.KillUnitWithoutSpecial();
		StartCoroutine(Gravitate());
	}
	
	private IEnumerator SwitchInvalid(Node first, Node second)
	{
		var f = first.GetUnitBehaviour();
		var s = second.GetUnitBehaviour();
		StartCoroutine(f.MoveEnumerator(first));
		StartCoroutine(s.MoveEnumerator(second));
		while (_currentGameState==GameStates.Animated)
		{
			yield return null;
		}
		StartCoroutine(f.MoveEnumerator(second));
		StartCoroutine(s.MoveEnumerator(first));
		while (_currentGameState==GameStates.Animated)
		{
			yield return null;
		}
		var y = first.GetUnitBehaviour();
		first.SetUnitBehaviour(second.GetUnitBehaviour());
		second.SetUnitBehaviour(y);
	}

	public void OnGameStateChanged(GameStates gameState)
	{
		switch (gameState)
		{
			case GameStates.Animated:
				break;
			case GameStates.WaitAction:
				if (_currentGameState == GameStates.EditMode)
				{
					GenerateNewUnits();
				}
				break;
			case GameStates.EditMode:
				CleanNodesImmediately();
				break;
			default:
				throw new ArgumentOutOfRangeException("gameState", gameState, null);
		}
		_currentGameState = gameState;
	}
}
