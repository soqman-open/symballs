﻿using UnityEngine;

public static class Utils {
	
	public static bool IsNeighbours(ISelectable first, ISelectable second)
	{
		if (first == null || second == null) return false;
		return Mathf.Abs(first.GetX() - second.GetX()) == 1 && first.GetY() == second.GetY() || Mathf.Abs(first.GetY() - second.GetY()) == 1 && first.GetX() == second.GetX();
	}
	public static bool IsNeighbours9(ISelectable first, ISelectable second)
	{
		if (first == null || second == null) return false;
		return Mathf.Abs(first.GetX() - second.GetX()) <= 1 && Mathf.Abs(first.GetY() - second.GetY()) <= 1 && !first.Equals(second);
	}
}
