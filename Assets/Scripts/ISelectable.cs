﻿public interface ISelectable
{
	void Select();
	void SelectAsNeigbor();
	void DeSelect();
	void OnPointerDown();
	void OnPointerEnter();
	int GetX();
	int GetY();
}
