﻿using UnityEngine;
[CreateAssetMenu(fileName = "New UnitType", menuName = "UnitType", order = 51)]
public class UnitType : ScriptableObject
{
    public Color Color;
    public Sprite SpriteImage;
    
    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    public override bool Equals(object other)
    {
        if (other == null || GetType() != other.GetType())return false;
        var unitType = (UnitType) other;
        return Color.Equals(unitType.Color);
    }
}
