﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class StatsHolder : Singleton<StatsHolder>, IGameStateListener
{
	private int _points;
	private int _moves;
	private float _currentTimer;
	private bool _isTimerRunning;
	private int _levelTime=60;
	private int _levelMoves=30;
	private int _levelPoints=30;
	private int _countdown;
	private bool _hasWin;
	private bool _hasTime;
	private bool _hasMoves;
	public UnityEvent OnLevelEndsGoodEvent;
	public UnityEvent OnLevelEndsBadEvent;
	private GameStates _currentGameState=GameStates.EditMode;

	[SerializeField] private UnityEngine.UI.Text _timerUiText;
	[SerializeField] private UnityEngine.UI.Text _movesUiText;
	[SerializeField] private UnityEngine.UI.Text _pointsUiText;

	private void Start()
	{
		GameStateManager.Instance.RegisterGameStateListener(this);
	}

	private void OnDestroy()
	{
		if (GameStateManager.Instance != null) GameStateManager.Instance.UnRegisterGameStateListener(this);
	}

	public int Moves
	{
		get { return _moves; }
		set
		{
			_moves = value;
			if (_hasMoves)
			{
				_movesUiText.text = (_levelMoves - _moves).ToString();
				if(_levelMoves <= _moves)LevelEnd();
			}
		}
	}

	private void LevelEnd()
	{
		GameStateManager.Instance.EditModeTrigger();
		if (_points < _levelPoints)
		{
			OnLevelEndsBadEvent.Invoke();
			
		}
		else
		{
			OnLevelEndsGoodEvent.Invoke();
		}
	}

	private void ResetStats()
	{
		_points = 0;
		_moves = 0;
		_movesUiText.text = _hasMoves?_levelMoves.ToString():"-";
		_timerUiText.text = _hasTime?_levelTime.ToString():"-";
		_pointsUiText.text = "0";
	}
	public string LevelTime
	{
		set { _levelTime = int.Parse(value); }
	}

	public string LevelMoves
	{
		set { _levelMoves = int.Parse(value); }
	}
	
	public string LevelPoints
	{
		set { _levelPoints = int.Parse(value); }
	}

	public bool HasTime
	{
		set { _hasTime = value; }
	}

	public bool HasMoves
	{
		set { _hasMoves = value; }
	}

	public bool HasWin
	{
		set { _hasWin = value; }
	}

	public void PointsEarned(int count)
	{
		if (count == 0) return;
		_points += count;
		_pointsUiText.text = _points.ToString();
		Debug.Log("PointsEarned "+count);
		if (_hasWin&&_points >= _levelPoints)
		{
			GameStateManager.Instance.EditModeTrigger();
			OnLevelEndsGoodEvent.Invoke();
		}
	}

	public void StartTimer()
	{
		_isTimerRunning = true;
		StartCoroutine(UpdateTimer());
	}

	private void StopTimer()
	{
		_isTimerRunning = false;
		_currentTimer = 0;
		_countdown = 0;
	}
	
	public void PauseTimer()
	{
		_isTimerRunning = false;
	}

	private IEnumerator UpdateTimer()
	{
		while (_isTimerRunning)
		{
			_currentTimer += Time.deltaTime;
			if (_hasTime)
			{
				_countdown = (int) (_levelTime - _currentTimer);
				_timerUiText.text = _countdown.ToString();
				if (_currentTimer >= _levelTime)
				{
					Debug.Log("TimerEnds");
					if (_hasTime) LevelEnd();
					_isTimerRunning = false;
				}
			}
			yield return null;
		}
	}

	public void OnGameStateChanged(GameStates gameState)
	{
		switch (gameState)
		{
			case GameStates.EditMode:
				StopTimer();
				break;
			case GameStates.WaitAction:
				if (_currentGameState == GameStates.EditMode)
				{
					ResetStats();
					if(_hasTime)StartTimer();
				}
				break;
			case GameStates.Animated:
				break;
			default:
				throw new ArgumentOutOfRangeException("gameState", gameState, null);
		}
		_currentGameState = gameState;
	}
}
