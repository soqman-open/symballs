﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameStateManager : Singleton<GameStateManager>
{
	private GameStates _gameState=GameStates.EditMode;
	private readonly List<IGameStateListener> _gameStateListeners=new List<IGameStateListener>();
	private readonly List<IAnimable> _animables = new List<IAnimable>();

	public UnityEvent OnGameStartEvent;
	public UnityEvent OnGameStopEvent;
	

	private void Start()
	{
		EditModeTrigger();
	}

	public GameStates GameState
	{
		get { return _gameState; }
	}

	public void PauseTrigger()
	{
		Time.timeScale = 0;
	}
	
	public void UnPauseTrigger()
	{
		Time.timeScale = 1;
	}
	
	public void EditModeTrigger()
	{
		_gameState = GameStates.EditMode;
		GameStatusChangedMessage();
		UnPauseTrigger();
		OnGameStopEvent.Invoke();
	}
	
	private void AnimationRunTrigger()
	{
		_gameState = GameStates.Animated;
		GameStatusChangedMessage();
	}
	
	public void WaitActionTrigger()
	{
		if (_gameState == GameStates.EditMode)
		{
			OnGameStartEvent.Invoke();
		}
		_gameState = GameStates.WaitAction;
		GameStatusChangedMessage();
	}

	private void GameStatusChangedMessage()
	{
		if (_gameStateListeners.Count == 0) return;
		foreach (var gameStateListener in _gameStateListeners)
		{
			gameStateListener.OnGameStateChanged(_gameState);
		}
	}
	
	public void RegisterAnimation(IAnimable animable)
	{
		_animables.Add(animable);
		AnimationRunTrigger();
	}
	
	public void UnRegisterAnimation(IAnimable animable)
	{
		if (!_animables.Contains(animable))return;
		_animables.Remove(animable);
		if (_animables.Count == 0 && _gameState == GameStates.Animated)
		{
			WaitActionTrigger();
		}
		
	}
	
	public void RegisterGameStateListener(IGameStateListener listener)
	{
		_gameStateListeners.Add(listener);
	}
	
	public void UnRegisterGameStateListener(IGameStateListener listener)
	{
		_gameStateListeners.Remove(listener);
	}
}
