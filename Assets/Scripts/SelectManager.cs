﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class SelectManager : Singleton<SelectManager>, IGameStateListener
{
	private bool _isSelectablesPaused;
	public ISelectable SelectedFirst;
	public ISelectable SelectedSecond;
	
	private readonly List<ISelectable> _selectables=new List<ISelectable>();

	private void Start()
	{
		GameStateManager.Instance.RegisterGameStateListener(this);
	}

	private void OnDisable()
	{
		if(GameStateManager.Instance!=null)
		GameStateManager.Instance.UnRegisterGameStateListener(this);
	}

	private void Pause()
	{
		_isSelectablesPaused = true;
	}
	
	private void UnPause()
	{
		_isSelectablesPaused = false;
	}

	public void RegisterSelectable(ISelectable selectable)
	{
		_selectables.Add(selectable);
	}
	
	public void UnRegisterSelectable(ISelectable selectable)
	{
		_selectables.Remove(selectable);
	}
	
	public void OnPointerDown(ISelectable selectable)
	{
		if (_isSelectablesPaused||selectable == SelectedFirst) return;
		if (Utils.IsNeighbours(selectable, SelectedFirst))
		{
			SelectedSecond = selectable;
			selectable.SelectAsNeigbor();
			return;
		}
		SelectFirst(selectable);
		DeSelectExcept(selectable);
	}

	public void OnPointerEnter(ISelectable selectable)
	{
#if UNITY_STANDALONE || UNITY_EDITOR
		if (_isSelectablesPaused||SelectedFirst == null || SelectedFirst==selectable || SelectedSecond != null || !Input.GetMouseButton(0)) return;
#elif UNITY_ANDROID || UNITY_IOS
		if (IsSelectablesPaused||SelectedFirst == null || SelectedFirst==selectable || SelectedSecond != null || Input.touchCount == 0) return;
#endif
		
		if (!Utils.IsNeighbours(selectable, SelectedFirst)) return;
		SelectSecond(selectable);
	}

	private void DeSelectExcept(ISelectable selectable)
	{
		SelectedFirst = selectable;
		SelectedSecond = null;
		foreach (var s in _selectables.Where(x=>x!=selectable))
		{
			s.DeSelect();
		}
	}
	
	public void DeSelectAll()
	{
		SelectedFirst = null;
		SelectedSecond = null;
		foreach (var s in _selectables)
		{
			s.DeSelect();
		}
	}

	private void SelectFirst(ISelectable selectable)
	{
		SelectedFirst = selectable;
		selectable.Select();
	}
	
	private void SelectSecond(ISelectable selectable)
	{
		SelectedSecond = selectable;
		selectable.SelectAsNeigbor();
	}

	public void OnGameStateChanged(GameStates gameState)
	{
		if (gameState.Equals(GameStates.WaitAction)) UnPause();
		if (gameState.Equals(GameStates.Animated)) Pause();
	}
}
