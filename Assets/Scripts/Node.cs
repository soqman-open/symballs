﻿using System;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

public class Node : MonoBehaviour, ISelectable
{
	private UnitBehaviour _unitBehaviour;

	private void Awake()
	{
		if (SelectManager.Instance != null)
			SelectManager.Instance.RegisterSelectable(this);
	}

	private void OnDestroy()
	{
		if (SelectManager.Instance != null)
			SelectManager.Instance.UnRegisterSelectable(this);
	}

	public void SetUnitBehaviour(UnitBehaviour unitBehaviour)
	{
		if(unitBehaviour==null)return;
		_unitBehaviour = unitBehaviour;
		IsEngaged = true;
	}

	public void RemoveUnitBehaviour()
	{
		_unitBehaviour = null;
		IsEngaged = false;
	}

	public UnitBehaviour GetUnitBehaviour()
	{
		return _unitBehaviour;
	}

	public int Y { private get; set; }

	public int X { private get; set; }

	public bool IsEngaged;

	public bool IsMatch3(UnitType unitType)
	{
		if (CompareUnitType(X-1, Y,unitType))
		{
			if (CompareUnitType(X - 2, Y,unitType)) return true;
			if (CompareUnitType(X+1, Y,unitType)) return true;
		}
		if (CompareUnitType(X + 1, Y,unitType))
		{
			if (CompareUnitType(X + 2, Y,unitType)) return true;
		}
		if (CompareUnitType(X, Y-1,unitType))
		{
			if (CompareUnitType(X, Y-2,unitType)) return true;
			if (CompareUnitType(X, Y+1,unitType)) return true;
		}
		if (CompareUnitType(X, Y+1,unitType))
		{
			if (CompareUnitType(X, Y+2,unitType)) return true;
		}
		return false;
	}
	
	public bool IsMatch3()
	{
		return IsMatch3(_unitBehaviour.Unit.UnitType);
	}

	public void CheckNeighbours()
	{
		if (CompareUnitType(X,Y-1,GetUnitBehaviour().Unit.UnitType) && CompareUnitType(X,Y+1,GetUnitBehaviour().Unit.UnitType))
		{
			Area.Matrix[X, Y - 1].GetUnitBehaviour().IsMarkedForKill = true;
			Area.Matrix[X, Y + 1].GetUnitBehaviour().IsMarkedForKill = true;
			GetUnitBehaviour().IsMarkedForKill = true;
		}
		if (CompareUnitType(X-1,Y,GetUnitBehaviour().Unit.UnitType) && CompareUnitType(X+1,Y,GetUnitBehaviour().Unit.UnitType))
		{
			Area.Matrix[X-1, Y].GetUnitBehaviour().IsMarkedForKill = true;
			Area.Matrix[X+1, Y].GetUnitBehaviour().IsMarkedForKill = true;
			GetUnitBehaviour().IsMarkedForKill = true;
		}
	}

	public bool CheckPossible()
	{
		if (Area.Instance.CheckExistAndHaveUnit(X,Y+1))
		{
			if(CompareUnitType(X,Y+2) && CompareUnitType(X,Y+3))
			{
				GetUnitBehaviour().SetMarkedAsPossible(true);
				Area.Matrix[X,Y+2].GetUnitBehaviour().SetMarkedAsPossible(true);
				Area.Matrix[X,Y+3].GetUnitBehaviour().SetMarkedAsPossible(true);
			}
			if(CompareUnitType(X-1,Y+1) && CompareUnitType(X-2,Y+1))
			{
				GetUnitBehaviour().SetMarkedAsPossible(true);
				Area.Matrix[X-1,Y+1].GetUnitBehaviour().SetMarkedAsPossible(true);
				Area.Matrix[X-2,Y+1].GetUnitBehaviour().SetMarkedAsPossible(true);
			}
			if(CompareUnitType(X+1,Y+1) && CompareUnitType(X+2,Y+1))
			{
				GetUnitBehaviour().SetMarkedAsPossible(true);
				Area.Matrix[X+1,Y+1].GetUnitBehaviour().SetMarkedAsPossible(true);
				Area.Matrix[X+2,Y+1].GetUnitBehaviour().SetMarkedAsPossible(true);
			}
			if(CompareUnitType(X-1,Y+1) && CompareUnitType(X+1,Y+1))
			{
				GetUnitBehaviour().SetMarkedAsPossible(true);
				Area.Matrix[X-1,Y+1].GetUnitBehaviour().SetMarkedAsPossible(true);
				Area.Matrix[X+1,Y+1].GetUnitBehaviour().SetMarkedAsPossible(true);
			}
		}
		if (Area.Instance.CheckExistAndHaveUnit(X+1,Y))
		{
			if(CompareUnitType(X+2,Y) && CompareUnitType(X+3,Y))
			{
				GetUnitBehaviour().SetMarkedAsPossible(true);
				Area.Matrix[X+2,Y].GetUnitBehaviour().SetMarkedAsPossible(true);
				Area.Matrix[X+3,Y].GetUnitBehaviour().SetMarkedAsPossible(true);
			}
			if(CompareUnitType(X+1,Y+1) && CompareUnitType(X+1,Y+2))
			{
				GetUnitBehaviour().SetMarkedAsPossible(true);
				Area.Matrix[X+1,Y+1].GetUnitBehaviour().SetMarkedAsPossible(true);
				Area.Matrix[X+1,Y+2].GetUnitBehaviour().SetMarkedAsPossible(true);
			}
			if(CompareUnitType(X+1,Y-1) && CompareUnitType(X+1,Y-2))
			{
				GetUnitBehaviour().SetMarkedAsPossible(true);
				Area.Matrix[X+1,Y-1].GetUnitBehaviour().SetMarkedAsPossible(true);
				Area.Matrix[X+1,Y-2].GetUnitBehaviour().SetMarkedAsPossible(true);
			}
			if(CompareUnitType(X+1,Y+1) && CompareUnitType(X+1,Y-1))
			{
				GetUnitBehaviour().SetMarkedAsPossible(true);
				Area.Matrix[X+1,Y+1].GetUnitBehaviour().SetMarkedAsPossible(true);
				Area.Matrix[X+1,Y-1].GetUnitBehaviour().SetMarkedAsPossible(true);
			}
		}
		if (Area.Instance.CheckExistAndHaveUnit(X,Y-1))
		{
			if(CompareUnitType(X,Y-2) && CompareUnitType(X,Y-3))
			{
				GetUnitBehaviour().SetMarkedAsPossible(true);
				Area.Matrix[X,Y-2].GetUnitBehaviour().SetMarkedAsPossible(true);
				Area.Matrix[X,Y-3].GetUnitBehaviour().SetMarkedAsPossible(true);
			}
			if(CompareUnitType(X-1,Y-1) && CompareUnitType(X-2,Y-1))
			{
				GetUnitBehaviour().SetMarkedAsPossible(true);
				Area.Matrix[X-1,Y-1].GetUnitBehaviour().SetMarkedAsPossible(true);
				Area.Matrix[X-2,Y-1].GetUnitBehaviour().SetMarkedAsPossible(true);
			}
			if(CompareUnitType(X+1,Y-1) && CompareUnitType(X+2,Y-1))
			{
				GetUnitBehaviour().SetMarkedAsPossible(true);
				Area.Matrix[X+1,Y-1].GetUnitBehaviour().SetMarkedAsPossible(true);
				Area.Matrix[X+2,Y-1].GetUnitBehaviour().SetMarkedAsPossible(true);
			}
			if(CompareUnitType(X-1,Y-1) && CompareUnitType(X+1,Y-1))
			{
				GetUnitBehaviour().SetMarkedAsPossible(true);
				Area.Matrix[X-1,Y-1].GetUnitBehaviour().SetMarkedAsPossible(true);
				Area.Matrix[X+1,Y-1].GetUnitBehaviour().SetMarkedAsPossible(true);
			}
		}
		if (Area.Instance.CheckExistAndHaveUnit(X-1,Y))
		{
			if(CompareUnitType(X-2,Y) && CompareUnitType(X-3,Y))
			{
				GetUnitBehaviour().SetMarkedAsPossible(true);
				Area.Matrix[X-2,Y].GetUnitBehaviour().SetMarkedAsPossible(true);
				Area.Matrix[X-3,Y].GetUnitBehaviour().SetMarkedAsPossible(true);
			}
			if(CompareUnitType(X-1,Y-1) && CompareUnitType(X-1,Y-2))
			{
				GetUnitBehaviour().SetMarkedAsPossible(true);
				Area.Matrix[X-1,Y-1].GetUnitBehaviour().SetMarkedAsPossible(true);
				Area.Matrix[X-1,Y-2].GetUnitBehaviour().SetMarkedAsPossible(true);
			}
			if(CompareUnitType(X-1,Y+1) && CompareUnitType(X-1,Y+2))
			{
				GetUnitBehaviour().SetMarkedAsPossible(true);
				Area.Matrix[X-1,Y+1].GetUnitBehaviour().SetMarkedAsPossible(true);
				Area.Matrix[X-1,Y+2].GetUnitBehaviour().SetMarkedAsPossible(true);
			}
			if(CompareUnitType(X-1,Y-1) && CompareUnitType(X-1,Y+1))
			{
				GetUnitBehaviour().SetMarkedAsPossible(true);
				Area.Matrix[X-1,Y-1].GetUnitBehaviour().SetMarkedAsPossible(true);
				Area.Matrix[X-1,Y+1].GetUnitBehaviour().SetMarkedAsPossible(true);
			}
		}
		return GetUnitBehaviour().GetIsPossible();
	}

	public int  KillChain(UnitType type)
	{
		IsEngaged = false;
		var countX = 0;
		var countY = 0;
		var a=CompareUnitType(X - 2, Y, type);
		var b=CompareUnitType(X - 1, Y, type);
		var c=CompareUnitType(X + 1, Y, type);
		var d=CompareUnitType(X + 2, Y, type);
		if (a && b && c && d)
		{
			if(Area.Matrix[X-2,Y].KillUnit())countX++;
			if(Area.Matrix[X-1,Y].KillUnit())countX++;
			if(Area.Matrix[X+1,Y].KillUnit())countX++;
			if(Area.Matrix[X+2,Y].KillUnit())countX++;
			
		}
		else if (a && b && c)
		{
			if(Area.Matrix[X-2,Y].KillUnit())countX++;
			if(Area.Matrix[X-1,Y].KillUnit())countX++;
			if(Area.Matrix[X+1,Y].KillUnit())countX++;
		}
		else if (b && c && d)
		{
			if(Area.Matrix[X-1,Y].KillUnit())countX++;
			if(Area.Matrix[X+1,Y].KillUnit())countX++;
			if(Area.Matrix[X+2,Y].KillUnit())countX++;
		}
		else if (a && b)
		{
			if(Area.Matrix[X-2,Y].KillUnit())countX++;
			if(Area.Matrix[X-1,Y].KillUnit())countX++;
		}
		else if (b && c)
		{
			if(Area.Matrix[X-1,Y].KillUnit())countX++;
			if(Area.Matrix[X+1,Y].KillUnit())countX++;
		}
		else if (c && d)
		{
			if(Area.Matrix[X+1,Y].KillUnit())countX++;
			if(Area.Matrix[X+2,Y].KillUnit())countX++;
		}
		a=CompareUnitType(X, Y-2, type);
		b=CompareUnitType(X, Y-1, type);
		c=CompareUnitType(X, Y+1, type);
		d=CompareUnitType(X, Y+2, type);
		if (a && b && c && d)
		{
			if(Area.Matrix[X,Y-2].KillUnit())countY++;
			if(Area.Matrix[X,Y-1].KillUnit())countY++;
			if(Area.Matrix[X,Y+1].KillUnit())countY++;
			if(Area.Matrix[X,Y+2].KillUnit())countY++;
		}
		else if (a && b && c)
		{
			if(Area.Matrix[X,Y-2].KillUnit())countY++;
			if(Area.Matrix[X,Y-1].KillUnit())countY++;
			if(Area.Matrix[X,Y+1].KillUnit())countY++;
		}
		else if (b && c && d)
		{
			if(Area.Matrix[X,Y-1].KillUnit())countY++;
			if(Area.Matrix[X,Y+1].KillUnit())countY++;
			if(Area.Matrix[X,Y+2].KillUnit())countY++;
		}
		else if (a && b)
		{
			if(Area.Matrix[X,Y-2].KillUnit())countY++;
			if(Area.Matrix[X,Y-1].KillUnit())countY++;
		}
		else if (b && c)
		{
			if(Area.Matrix[X,Y-1].KillUnit())countY++;
			if(Area.Matrix[X,Y+1].KillUnit())countY++;
		}
		
		else if (c && d)
		{
			if(Area.Matrix[X,Y+1].KillUnit())countY++;
			if(Area.Matrix[X,Y+2].KillUnit())countY++;
		}
		IsEngaged = true;
		if (countX == 0 && countY == 0)return 0;
		if (countX >= 3 || countY >= 3)ChangeUnitToSpecial(countX,countY);
		else KillUnit();
		return countX+countY+1;
	}

	private void ChangeUnitToSpecial(int xcount, int ycount)
	{
		var ut = GetUnitBehaviour().Unit.UnitType;
		GetUnitBehaviour().DestroyObject();
		RemoveUnitBehaviour();
		var specialUnit=(Unit.SpecialUnit)Random.Range(1,4);
		if (xcount + ycount + 1 > 4)
		{
			specialUnit = Unit.SpecialUnit.Rocket;
			ut = null;
		}
		SetUnitBehaviour(Area.Instance.CreateUnitWithSpecial(X,Y,ut,specialUnit));
	}
	
	public bool KillUnit()
	{
		if (!IsEngaged||GetUnitBehaviour()==null) return false;
		IsEngaged = false;
		switch (GetUnitBehaviour().Unit.Special)
		{
			case Unit.SpecialUnit.None:
				break;
			case Unit.SpecialUnit.XRemover:
				Area.Instance.KillX(X);
				break;
			case Unit.SpecialUnit.Bomb3:
				Area.Instance.KillRange3(X,Y);
				break;
			case Unit.SpecialUnit.Yremover:
				Area.Instance.KillY(Y);
				break;
			case Unit.SpecialUnit.Rocket:
				break;
			default:
				throw new ArgumentOutOfRangeException();
		}
		GetUnitBehaviour().Kill();
		RemoveUnitBehaviour();
		return true;
	}
	
	public void KillUnitWithoutSpecial()
	{
		if (!IsEngaged) return;
		GetUnitBehaviour().Kill();
		RemoveUnitBehaviour();
	}
	
	private static bool CompareUnitType(int x, int y, UnitType unitType)
	{
		if (!Area.Instance.CheckExistAndHaveUnit(x, y)) return false;
		var secondType = Area.Matrix[x, y].GetUnitBehaviour().Unit.UnitType;
		return secondType.Equals(unitType);
	}
	
	private bool CompareUnitType(int x, int y)
	{
		return CompareUnitType(x, y, GetUnitBehaviour().Unit.UnitType);
	}

	[SerializeField ]private UnityEvent _onSelectEvent;
	[SerializeField ]private UnityEvent _onDeSelectEvent;
	

	public void Select()
	{
		_onSelectEvent.Invoke();
	}
	
	public void DeSelect()
	{
		_onDeSelectEvent.Invoke();
	}

	public void SelectAsNeigbor()
	{
		Area.Instance.TrySwitch();
	}

	public void OnPointerDown()
	{
		if (GameStateManager.Instance.GameState == GameStates.EditMode && Area.Instance.CanRemoveNode(X,Y))
		{
			Area.Instance.GenerateEmptyNode(X,Y);
			Destroy(gameObject);
		}
		else if(GameStateManager.Instance.GameState == GameStates.WaitAction)
		{
			SelectManager.Instance.OnPointerDown(this);
		}
	}
	
	public void OnPointerEnter()
	{
		SelectManager.Instance.OnPointerEnter(this);
	}

	public int GetX()
	{
		return X;
	}

	public int GetY()
	{
		return Y;
	}

	public override bool Equals(object other)
	{
		return base.Equals(other);
	}

	protected bool Equals(Node other)
	{
		return base.Equals(other) && Y == other.Y && X == other.X;
	}

	public override int GetHashCode()
	{
		unchecked
		{
			var hashCode = base.GetHashCode();
			hashCode = (hashCode * 397) ^ Y;
			hashCode = (hashCode * 397) ^ X;
			return hashCode;
		}
	}
}
