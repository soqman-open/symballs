﻿using UnityEngine;
using UnityEngine.UI;

public class EditModeListener : MonoBehaviour, IGameStateListener {
	private void Awake()
	{
		GameStateManager.Instance.RegisterGameStateListener(this);
	}

	private void OnDestroy()
	{
		if(GameStateManager.Instance!=null)
		GameStateManager.Instance.UnRegisterGameStateListener(this);
	}

	public void OnGameStateChanged(GameStates gameState)
	{
		if (gameState.Equals(GameStates.EditMode))
		{
			var components = GetComponentsInChildren<Selectable>();
			foreach (var component in components)
			{
				component.interactable = true;
			}
		}
		else
		{
			var components = GetComponentsInChildren<Selectable>();
			foreach (var component in components)
			{
				component.interactable = false;
			}
		}
	}
}
