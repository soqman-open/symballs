﻿using System;
using JetBrains.Annotations;
using Random = UnityEngine.Random;

public class Unit
{
    private readonly int _id;
    private readonly UnitType _unitType;
    private SpecialUnit _special;

    public Unit(int id, [NotNull] UnitType unitType)
    {
        if (unitType == null) throw new ArgumentNullException("unitType");
        _id = id;
        _unitType = unitType;
        _special = SpecialUnit.None;
    }

    public UnitType UnitType
    {
        get { return _unitType; }
    }

    public SpecialUnit Special
    {
        get { return _special; }
    }

    public override bool Equals(object obj)
    {
        return obj!=null && obj.GetType()==GetType() && ((Unit) obj)._id==_id;
    }

    public override int GetHashCode()
    {
        return _id.GetHashCode();
    }
    
    public void SetSpecial(SpecialUnit specialUnit)
    {
        _special = specialUnit;
    }
    
    public enum SpecialUnit{None,XRemover,Bomb3,Yremover,Rocket}
}
